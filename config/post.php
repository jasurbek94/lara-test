<?php
return [
    'rss_feed'               => env('RSS_FEED', 'http://feeds.bbci.co.uk/news/rss.xml'),
    'post_valid_time'        => env('POST_VALID_TIME', 3600),
    'wish_search_parse_time' => env('WISH_SEARCH_POST_TIME', 1800),
    'per_page'               => env('POST_PAGE', 20),
];
