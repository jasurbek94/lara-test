<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WishSearch
 *
 * @property int $id
 * @property string $term
 * @property string|null $last_parsed_time
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|WishSearch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WishSearch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WishSearch query()
 * @method static \Illuminate\Database\Eloquent\Builder|WishSearch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WishSearch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WishSearch whereLastParsedTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WishSearch whereTerm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WishSearch whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class WishSearch extends Model
{
    //
}
