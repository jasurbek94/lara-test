<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CategoryPost
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryPost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryPost newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryPost query()
 * @mixin \Eloquent
 */
class CategoryPost extends Model
{
    //
}
