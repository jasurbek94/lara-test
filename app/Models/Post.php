<?php

namespace App\Models;

use Carbon\Carbon;
use App\DDD\Post\Filters\PostFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\News
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post query()
 * @mixin \Eloquent
 * @property int                             $id
 * @property string                          $title
 * @property string                          $slug
 * @property string|null                     $content
 * @property Carbon|null                     $last_parsed_time
 * @property int|null                        $created_by
 * @property string|null                     $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereLastParsedTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post filter($value)
 * @property Image                           $img
 * @property Image[]                         $images
 * @property string                          $image_link
 * @property string|null                     $pub_date
 * @property string|null                     $remote_link
 * @property string|null                     $description
 * @property string|null                     $guid
 * @property int                             $status
 * @property mixed|null                      $options
 * @property-read int|null                   $images_count
 * @method static \Illuminate\Database\Query\Builder|Post onlyTrashed()
 * @method static Builder|Post whereDescription($value)
 * @method static Builder|Post whereGuid($value)
 * @method static Builder|Post whereOptions($value)
 * @method static Builder|Post wherePubDate($value)
 * @method static Builder|Post whereRemoteLink($value)
 * @method static Builder|Post whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|Post withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Post withoutTrashed()
 */
class Post extends Model
{
    use SoftDeletes;

    protected $casts = [
        'options' => 'array',
    ];

    protected $appends = [
        'image_link',
    ];

    protected $dates = [
        'last_parsed_time',
    ];

    public function scopeFilter(Builder $builder, $request)
    {
        return (new PostFilter($request))->apply($builder);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function img()
    {
        return $this->hasOne(Image::class);
    }

    public function getImageLinkAttribute()
    {
        return $this->img ? $this->img->img_path : null;
    }

    public function isContentValid()
    {
        if (!$this->last_parsed_time) {
            return false;
        }

        return $this->last_parsed_time
            ->isAfter(Carbon::now()
                ->subSeconds(config('post.post_valid_time'))
            );
    }

    public function shouldReload()
    {
        if ($this->isChangedByUser()) {
            return false;
        }

        if (empty($this->content)) {
            return true;
        }

        return !$this->isContentValid();
    }

    public function isChangedByUser(): bool
    {
        if (!empty($this->options['is_user_changed'])) {
            return (bool)$this->options['is_user_changed'];
        }
        return false;
    }

    public function setIsUserChanged($data)
    {
        $options = $this->options;
        $options['is_user_changed'] = $data;
        $this->options = $options;
    }
}
