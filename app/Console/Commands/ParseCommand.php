<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DDD\Post\Parsers\RssParser;
use App\DDD\Post\Services\SearchService;

class ParseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse posts command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(SearchService $service)
    {
        $data = $service->search('dies');
        echo "<pre>";
        print_r($data);
        echo "<pre>";
        die;
    }
}
