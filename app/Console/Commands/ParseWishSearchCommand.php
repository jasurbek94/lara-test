<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DDD\Post\Repos\PostRepo;
use App\DDD\Post\Repos\WishSearchRepo;
use App\DDD\Post\Services\ParserService;

class ParseWishSearchCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:wishlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse wish list search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(ParserService $service, WishSearchRepo $repo)
    {
        $service->execute();
        $repo->deleteParsedData();
    }
}
