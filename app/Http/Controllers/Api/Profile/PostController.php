<?php


namespace App\Http\Controllers\Api\Profile;


use App\Models\Post;
use App\DDD\Post\VO\PostVo;
use Illuminate\Http\Request;
use App\DDD\Post\VO\PostRssVo;
use App\DDD\Post\Repos\PostRepo;
use App\Http\Requests\PostRequest;
use App\DDD\Post\VO\PostContentVo;
use Illuminate\Support\Facades\Auth;
use App\DDD\Post\Resources\PostResource;
use App\Http\Controllers\BaseApiController;
use App\DDD\Post\Resources\PostListResource;
use App\DDD\Post\Services\PostCreateService;
use App\DDD\Post\Services\PostContentParserService;

class PostController extends BaseApiController
{
    /**
     * @var PostRepo
     */
    private $repo;

    public function __construct(PostRepo $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request, PostRepo $repo)
    {
        $list = $repo->list($request->all());

        return new PostListResource($list);
    }

    public function show(PostContentParserService $service, $id)
    {
        $post = $this->repo->findOneById($id);
        $service->parseFromPost($post);
        return new PostResource($post);
    }

    public function store(PostRequest $request, PostCreateService $service)
    {
        $post = new PostRssVo(
            $request->input('title'),
            $request->input('description')
        );

        $postContentVo = new PostContentVo(
            $request->input('content')
        );

        $postVo = new PostVo($post, $postContentVo);
        $post = $service->save($postVo, Auth::getUser());
        $service->image($post, $request->file('img'));
        return new PostResource($post);
    }

    public function update(PostRequest $request, PostCreateService $service, Post $post)
    {
        $postRssVo = new PostRssVo(
            $request->input('title'),
            $request->input('description')
        );

        $postContentVo = new PostContentVo(
            $request->input('content')
        );


        $service->image($post, $request->file('img'));
        return new PostResource($post);
    }
}
