<?php


namespace App\Http\Controllers\Api;


use App\DDD\Post\Services\SearchService;
use App\DDD\Post\Resources\PostResource;
use App\Http\Controllers\BaseApiController;

class PostViewController extends BaseApiController
{
    /**
     * @var SearchService
     */
    private $service;

    /**
     * PostViewController constructor.
     *
     * @param SearchService $service
     */
    public function __construct(SearchService $service)
    {
        $this->service = $service;
    }

    public function view($term)
    {
        $post = $this->service->search($term);

        if (!$post){
            return  $this->respondNotFound('Term not found');
        }
        return new PostResource($post);
    }
}
