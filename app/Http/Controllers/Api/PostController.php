<?php


namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\DDD\Post\Repos\PostRepo;
use App\DDD\Post\Resources\PostResource;
use App\Http\Controllers\BaseApiController;
use App\DDD\Post\Resources\PostListResource;
use App\DDD\Post\Services\PostContentParserService;

class PostController extends BaseApiController
{
    /**
     * @var PostRepo
     */
    private $repo;

    /**
     * PostController constructor.
     *
     * @param PostRepo $repo
     */
    public function __construct(PostRepo $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        $list = $this->repo->list($request->all());

        return new PostListResource($list);
    }

    public function show(PostContentParserService $service, $id)
    {
        $post = $this->repo->findOneById($id);
        $service->parseFromPost($post);
        return new PostResource($post);
    }
}
