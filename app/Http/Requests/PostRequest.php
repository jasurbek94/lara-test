<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => [
                'required',
                'string',
                Rule::unique('posts', 'id')->ignore($this->query('id')),
            ],
            'description' => 'required|string',
            'content'     => 'nullable',
            'img'         => 'nullable|image',
        ];
    }
}
