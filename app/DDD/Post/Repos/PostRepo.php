<?php


namespace App\DDD\Post\Repos;


use Carbon\Carbon;
use App\Models\Post;
use App\Models\Image;
use App\DDD\Post\VO\PostVo;
use Illuminate\Support\Str;
use App\DDD\Post\VO\PostContentVo;
use App\DDD\Post\Interfaces\IPostRepo;
use Illuminate\Database\Eloquent\Builder;

class PostRepo implements IPostRepo
{
    public function findOneWithTrashed($term)
    {
        return Post::withTrashed()
            ->where('title', 'like', '%' . $term . '%')
            ->first();
    }

    public function findOneById($id)
    {
        return Post::withTrashed()
            ->where('id', $id)
            ->firstOrFail();
    }

    public function savePostWithPostVo(PostVo $vo)
    {
        $post = new Post();
        $post->title = $vo->getTitle();
        $post->description = $vo->getDescription();
        $post->content = $vo->getContent();
        $post->pub_date = $vo->getPubDate();
        $post->remote_link = $vo->getLink();
        $post->guid = $vo->getGuid();
        $post->slug = Str::slug($post->title);
        $post->save();

        if ($vo->getImg()) {
            $this->saveImage($post, $vo->getImg());
        }
        return $post;
    }

    public function savePostConentWithImage(Post $post, PostContentVo $vo)
    {
        $post->content = $vo->getContent();
        if ($vo->getImage()) {
            $this->saveImage($post, $vo->getImage());
        }
        $post->last_parsed_time = Carbon::now();
        $post->save();
        return $post;
    }

    protected function saveImage(Post $post, $link)
    {
        $image = new Image();
        $image->img_path = $link;
        $post->img()->save($image);
    }

    public function findPostByGuid($guid)
    {
        return Post::withTrashed()
            ->where('guid', $guid)
            ->first();
    }

    public function list($data)
    {
        return Post::filter($data)
            ->paginate($data['perPage'] ?? config('post.per_page'));
    }
}
