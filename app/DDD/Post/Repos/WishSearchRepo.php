<?php


namespace App\DDD\Post\Repos;


use Carbon\Carbon;
use App\Models\Post;
use App\Models\WishSearch;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class WishSearchRepo
{
    public function findByTerm($term)
    {
        return WishSearch::query()
            ->where('term', '%' . $term . '%')
            ->orWhere('term', $term)
            ->first();
    }

    public function saveTerm($term)
    {
        $model = new WishSearch();
        $model->term = $term;
        $model->save();
        return $model;
    }

    public function getTermsCount()
    {
        return WishSearch::query()
            ->where(
                'last_parsed_time',
                '<',
                Carbon::now()
                    ->subSeconds(config('post.wish_search_parse_time'))
            )->count();
    }

    public function deleteParsedData()
    {
        //select id from wish_searches where exists (SELECT * FROM posts where posts.title like CONCAT('%',wish_searches.term, "%") limit 1)

        $idList = DB::select("select id from wish_searches where exists
    (SELECT * FROM posts where posts.title like CONCAT('%',wish_searches.term, '%') limit 1)");

        $ids = Arr::pluck($idList, 'id');

        if (!$ids) {
            return true;
        }

        return WishSearch::whereIn('id', $ids)->delete();
    }
}
