<?php


namespace App\DDD\Post\Parsers;


use App\DDD\Post\VO\PostVo;
use App\DDD\Post\VO\PostRssVo;
use Illuminate\Support\Collection;
use App\DDD\Post\VO\PostContentVo;

class PostsContentParser
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * PostsParser constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function parse()
    {
        $list = [];

        /**
         * @var $item PostRssVo
         */
        foreach ($this->collection as $item) {
            if (!$item instanceof PostRssVo) {
                continue;
            }

            $postContentVo = $this->parseContent($item);
            if ($postContentVo === null) {
                continue;
            }

            $list[] = new PostVo($item, $postContentVo);
        }

        return new Collection($list);
    }

    public function parseContent(PostRssVo $item): ?PostContentVo
    {
        return (new ParserPost($item->getLink()))->parse();
    }
}
