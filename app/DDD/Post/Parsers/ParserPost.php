<?php


namespace App\DDD\Post\Parsers;


use GuzzleHttp\Client;
use App\DDD\Post\VO\PostContentVo;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use App\DDD\Post\Exceptions\UrlNotProvidedException;

class ParserPost
{
    private $url;

    /**
     * ParserPost constructor.
     *
     * @param $url
     */
    public function __construct($url)
    {
        if (!$url) {
            throw new UrlNotProvidedException($url);
        }
        $this->url = $url;
    }

    public function parse(): ?PostContentVo
    {
        $client = new Client();
        try {
            $res = $client->get($this->url);
        } catch (ConnectException $exception) {
            return null;
        } catch (RequestException $exception) {
            return null;
        }

        return $this->parseContent($res->getBody()->getContents());
    }

    public function parseContent($htmlContent)
    {
        $html = str_get_html($htmlContent);
        if (!$html) {
            return null;
        }
        $main = $html->find('main', 0);
        if (!$main) {
            return null;
        }
        //article figure img
        $article = $main->find('article', 0);

        if (!$article) {
            return null;
        }

        $postContent = $article->innertext;

        $figure = $main->find('figure', 0);

        if (!$figure) {
            return new PostContentVo($postContent);
        }

        $img = $main->find('img', 0);

        if (!$img) {
            return null;
        }

        return new PostContentVo($postContent, $img->src);
    }
}
