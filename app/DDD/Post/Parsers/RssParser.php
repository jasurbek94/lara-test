<?php


namespace App\DDD\Post\Parsers;


use App\DDD\Post\VO\PostVo;
use App\DDD\Post\VO\PostRssVo;
use Illuminate\Support\Collection;

class RssParser
{
    /**
     * @param       $url
     * @param false $withoutContent
     *
     * @return Collection|PostVo[]|null
     *
     */
    public function parse($url, $withoutContent = false): ?Collection
    {
        $data = file_get_contents($url);
        $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, true);

        $list = $this->createVOFromArrayList($array);
        if ($withoutContent) {
            return $this->rssvoToPostVo($list);
        }

        return $this->parsePostContents($list);
    }

    protected function createVOFromArrayList($array)
    {
        $list = array_map([$this, 'arrayToVO'], $array['channel']['item'] ?? []);
        $list = array_filter($list);

        return new Collection($list);
    }

    protected function arrayToVO($postArray)
    {
        if (empty($postArray['pubDate']) && empty($postArray['guid'])) {
            return null;
        }
        return new PostRssVo(
            $postArray['title'] ?? '',
            $postArray['description'] ?? '',
            $postArray['link'] ?? '',
            $postArray['guid'] ?? '',
            $postArray['pubDate'] ?? ''
        );
    }

    private function parsePostContents(Collection $list)
    {
        return (new PostsContentParser($list))->parse();
    }

    /**
     * @param Collection $list
     */
    public function rssvoToPostVo(Collection $list)
    {
        return $list->map(function ($item) {
            return new PostVo($item);
        });
    }
}
