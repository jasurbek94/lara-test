<?php

namespace App\DDD\Post\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'title'       => $this->title,
            'pub_date'    => $this->pub_date,
            'remote_link' => $this->remote_link,
            'description' => $this->description,
            'content'     => $this->content,
            'img'         => $this->image_link,
        ];
    }
}
