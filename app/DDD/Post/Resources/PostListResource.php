<?php

namespace App\DDD\Post\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostListResource extends ResourceCollection
{
    public $collects = PostListViewResource::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }
}
