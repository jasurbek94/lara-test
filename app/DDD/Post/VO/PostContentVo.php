<?php


namespace App\DDD\Post\VO;


class PostContentVo
{
    private $data;
    private $image;

    /**
     * PostContentVo constructor.
     *
     * @param $data
     * @param $image
     */
    public function __construct($data, $image = null)
    {
        $this->data = $data;
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->data;
    }

    /**
     * @return mixed|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }
}
