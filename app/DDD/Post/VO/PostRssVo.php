<?php


namespace App\DDD\Post\VO;


use Carbon\Carbon;

class PostRssVo
{
    private $title;
    private $description;
    private $link;
    private $guid;
    private $pubDate;

    /**
     * PostVo constructor.
     *
     * @param $title
     * @param $description
     * @param $link
     * @param $guid
     * @param $pubDate
     */
    public function __construct($title, $description, $link = null, $guid = null, $pubDate = null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->link = $link;
        $this->guid = $guid;
        $this->pubDate = $pubDate;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return mixed
     */
    public function getPubDate()
    {
        return Carbon::createFromFormat('D, d M Y H:i:s e', $this->pubDate);
    }

}
