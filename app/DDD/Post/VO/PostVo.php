<?php


namespace App\DDD\Post\VO;


use Carbon\Carbon;

class PostVo
{
    /**
     * @var PostRssVo
     */
    private $rss_vo;
    /**
     * @var PostContentVo|null
     */
    private $content_vo;

    /**
     * PostVo constructor.
     *
     * @param PostRssVo          $rss_vo
     * @param PostContentVo|null $content_vo
     */
    public function __construct(PostRssVo $rss_vo, PostContentVo $content_vo = null)
    {
        $this->rss_vo = $rss_vo;
        $this->content_vo = $content_vo;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->rss_vo->getTitle();
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->rss_vo->getDescription();
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->rss_vo->getLink();
    }

    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->rss_vo->getGuid();
    }

    /**
     * @return mixed
     */
    public function getPubDate()
    {
        return $this->rss_vo->getPubDate();

    }

    public function getContent()
    {
        return $this->content_vo ? $this->content_vo->getContent() : null;
    }

    public function getImg()
    {
        return $this->content_vo ? $this->content_vo->getImage() : null;
    }

}
