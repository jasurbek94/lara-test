<?php


namespace App\DDD\Post\Filters;


use Carbon\Carbon;
use App\Classes\QueryFilter;

class PostFilter extends QueryFilter
{
    public function term($term)
    {
        $this->builder->where('title', $term);
    }

    public function dateFrom($term)
    {
        $this->builder->whereDate('pub_date', '>=', $this->readFromFormat($term)
            ->format('Y-m-d 00:00:00'));
    }

    public function dateTo($term)
    {
        $this->builder->whereDate('pub_date', '<=', $this->readFromFormat($term)
            ->format('Y-m-d 23:59:59'));
    }

    protected function readFromFormat($term)
    {
        return Carbon::createFromFormat('d.m.Y', $term);
    }
}
