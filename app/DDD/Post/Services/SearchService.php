<?php


namespace App\DDD\Post\Services;


use App\Models\Post;
use App\Models\WishSearch;
use App\DDD\Post\Repos\PostRepo;
use Illuminate\Support\Collection;
use App\DDD\Post\Parsers\RssParser;
use App\DDD\Post\Parsers\ParserPost;
use App\DDD\Post\Interfaces\IPostRepo;
use App\DDD\Post\Repos\WishSearchRepo;
use App\DDD\Post\Interfaces\IPostSearchInterface;
use App\DDD\Post\Exceptions\TrashedPostException;

class SearchService implements IPostSearchInterface
{
    /**
     * @var IPostRepo
     */
    private $repo;
    /**
     * @var WishSearchRepo
     */
    private $wish_search_repo;
    /**
     * @var PostSaverFromPostVoService
     */
    private $postSaverService;

    /**
     * SearchService constructor.
     *
     * @param IPostRepo $repo
     */
    public function __construct(PostRepo $repo, WishSearchRepo $wish_search_repo, PostSaverFromPostVoService $postSaverService)
    {
        $this->repo = $repo;
        $this->wish_search_repo = $wish_search_repo;
        $this->postSaverService = $postSaverService;
    }

    public function search($term)
    {
        $post = $this->searchFromDb($term);

        if ($this->isValidPost($post)) {
            return $this->returnPostFromDb($post);
        }

        $post = $this->getParsedItems($term);

        if ($this->isValidPost($post)) {
            return $this->returnPostFromDb($post);
        }

        $this->addToCron($term);

        return null;
    }

    protected function searchFromDb($data): ?Post
    {
        return $this->repo->findOneWithTrashed($data);
    }

    public function isValidPost(?Post $post)
    {
        if ($post && $post->trashed()) {
            throw new TrashedPostException();
        }

        return $post !== null;
    }

    private function returnPostFromDb(Post $post)
    {
        if (empty($post->content) || $post->shouldReload()) {
            $parser = new ParserPost($post->remote_link);
            $content = $parser->parse();
            if ($content) {
                $this->repo->savePostConentWithImage($post, $content);
            }
        }


        return $post;
    }

    /**
     * @return Collection
     */
    protected function getParsedItems($term)
    {
        $parser = new RssParser();
        $items = $parser->parse(config('post.rss_feed'), true);

        foreach ($items as $item) {
            $this->postSaverService->save($item);
        }

        return $this->repo->findOneWithTrashed($term);
    }

    public function addToCron($term)
    {
        if (!$this->wish_search_repo->findByTerm($term)) {
            $this->wish_search_repo->saveTerm($term);
        }
    }


}
