<?php


namespace App\DDD\Post\Services;


use App\DDD\Post\Repos\PostRepo;
use App\DDD\Post\Parsers\RssParser;

class ParserService
{
    /**
     * @var RssParser
     */
    private $parser;
    /**
     * @var PostSaverFromPostVoService
     */
    private $postSaverService;

    /**
     * ParserService constructor.
     *
     * @param RssParser                  $parser
     * @param PostSaverFromPostVoService $postSaverService
     */
    public function __construct(RssParser $parser, PostSaverFromPostVoService $postSaverService)
    {
        $this->parser = $parser;
        $this->postSaverService = $postSaverService;
    }

    public function execute()
    {
        $list = $this->parser->parse(config('post.rss_feed'));

        foreach ($list as $item) {
            $this->postSaverService->save($item);
        }
    }
}
