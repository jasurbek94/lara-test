<?php


namespace App\DDD\Post\Services;


use App\Models\Post;
use App\DDD\Post\VO\PostVo;
use App\DDD\Post\Repos\PostRepo;
use App\DDD\Post\VO\PostContentVo;
use App\DDD\Post\Parsers\ParserPost;

class PostContentParserService
{
    /**
     * @var PostRepo
     */
    private $repo;

    /**
     * PostContentParserService constructor.
     *
     * @param PostRepo $repo
     */
    public function __construct(PostRepo $repo)
    {
        $this->repo = $repo;
    }

    public function parseFromPost(Post $post, $force = false)
    {
        if (!$force && !$post->shouldReload()) {
            return $post;
        }

        $postContent = $this->parse($post->remote_link);
        if ($postContent) {
            $this->repo->savePostConentWithImage($post, $postContent);
        }
        return $postContent;
    }

    public function parseFromVo(PostVo $vo)
    {
        return $this->parse($vo->getLink());
    }

    public function parse($url): ?PostContentVo
    {
        $parser = new ParserPost($url);
        return $parser->parse();
    }
}
