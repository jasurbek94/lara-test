<?php


namespace App\DDD\Post\Services;


use App\User;
use App\Models\Post;
use App\Models\Image;
use App\DDD\Post\VO\PostVo;
use Illuminate\Support\Str;
use App\DDD\Post\Repos\PostRepo;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PostCreateService
{
    /**
     * @var PostRepo
     */
    private $repo;

    /**
     * PostCreateService constructor.
     *
     * @param PostRepo $repo
     */
    public function __construct(PostRepo $repo)
    {
        $this->repo = $repo;
    }

    public function save(PostVo $vo, User $user)
    {
        $post = $this->repo->savePostWithPostVo($vo);
        $post->created_by = $user->id;
        $post->setIsUserChanged(true);
        $post->save();
        return $post;
    }

    public function image(Post $post, UploadedFile $image)
    {
        $imageName = Str::random(32) . '.' . $image->clientExtension();
        $path = $image->storePublicly('posts', $imageName);

        $image = new Image();
        $image->img_path = Storage::url($path);
        $post->img()->save($image);
    }

    public function update(Post $post, PostVo $vo)
    {
        $post->setIsUserChanged(true);
        $post->save();
    }
}
