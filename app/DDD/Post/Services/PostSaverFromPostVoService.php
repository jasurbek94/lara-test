<?php


namespace App\DDD\Post\Services;


use App\DDD\Post\VO\PostVo;
use App\DDD\Post\Repos\PostRepo;

class PostSaverFromPostVoService
{
    /**
     * @var PostRepo
     */
    private $repo;

    /**
     * PostSaverFromPostVoService constructor.
     *
     * @param PostRepo $post_repo
     */
    public function __construct(PostRepo $post_repo)
    {
        $this->repo = $post_repo;
    }

    public function save(PostVo $vo)
    {
        if ($this->repo->findPostByGuid($vo->getGuid())) {
            return null;
        }
        return $this->repo->savePostWithPostVo($vo);
    }
}
