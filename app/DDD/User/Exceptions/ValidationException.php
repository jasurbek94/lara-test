<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-05-23
 * Time: 14:52
 */

namespace App\DDD\User\Exceptions;


use Throwable;

class ValidationException extends BaseExceptions
{
    /**
     * @var array
     */
    private $errors;

    public function __construct($errors = [], string $message = "Validation error", int $code = 0, Throwable $previous = null)
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
