<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGuidAndLinkToPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dateTime('pub_date')->nullable();
            $table->dropColumn('last_parsed_time');
            $table->string('remote_link')->nullable();
            $table->text('description')->nullable();
            $table->string('guid')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->json('options')->nullable();
        });

        Schema::table('posts', function (Blueprint $table) {

            $table->dateTime('last_parsed_time')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('last_parsed_time');
        });


        Schema::table('posts', function (Blueprint $table) {
            $table->dateTime('last_parsed_time');
            $table->dropColumn('pub_date');
            $table->dropColumn('remote_link');
            $table->dropColumn('description');
            $table->dropColumn('guid');
            $table->dropColumn('status');
            $table->dropColumn('options');
        });
    }
}
