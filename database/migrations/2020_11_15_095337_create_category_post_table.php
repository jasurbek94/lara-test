<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_post', function (Blueprint $table) {
            $table->bigInteger('category_id')->unsigned();
            $table->bigInteger('post_id')->unsigned();
            $table->timestamps();

            $table->foreign('post_id')
                ->on('posts')
                ->references('id');

            $table->foreign('category_id')
                ->on('categories')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('category_post', function (Blueprint $table) {
            $table->dropForeign('category_post_category_id_foreign');
            $table->dropForeign('category_post_post_id_foreign');
        });

        Schema::dropIfExists('category_post');
    }
}
