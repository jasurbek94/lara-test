<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Api')->name('api.')->middleware(['api'])->group(function () {


    Route::prefix('auth')->name('auth.')->group(function () {
        Route::post('register', 'RegisterController@register')->name('register');
        Route::post('login', 'AuthController@login')->name('login');
        Route::post('logout', 'AuthController@logout')->name('logout');
        Route::post('refresh', 'AuthController@refresh')->name('refresh');
    });

    Route::prefix('post')->name('post.')->group(function () {
        Route::post('search/{term}', 'PostViewController@view')->name('search');
        Route::get('/', 'PostController@index')->name('index');
        Route::get('{id}', 'PostController@show')->name('view');
    });

    Route::middleware('auth:api')->group(function () {

        Route::prefix('profile')
            ->namespace('profile')
            ->name('profile.')
            ->group(function () {
                Route::get('/', 'ProfileController@show')->name('show');
                Route::put('/', 'ProfileController@update')->name('update');

                Route::resource('/post', 'PostController');

            });

    });

});
